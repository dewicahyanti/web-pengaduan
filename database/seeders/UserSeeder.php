<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => "admin@gmail.com",
            'password' => Hash::make("password"),
            'role' => 'admin',
            'no_hp' => "088899992222",
            'alamat' => "Admin Street",
        ]);
            
        User::create([
            'name' => 'Dewi Cahyanti',
            'email' => "cahyanti@gmail.com",
            'password' => Hash::make("123"),
            'role' => 'user',
            'no_hp' => "083873568765",
            'alamat' => "Jln. Langgar",
        ]) ;
    }
}
