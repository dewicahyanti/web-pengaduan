@extends("layouts.app")
<!-- modal image popup -->
@section("title", "Selesai")
@section('custom-style')
<style>
    .no-wrap {
        /* text-overflow: ellipse;
        white-space: nowrap;
        overflow: hidden; */
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
</style>
@section("content")


<div class="modal fade" id="img" tabindex="-1" role="dialog" aria-labelledby="img">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img style="max-height:430px;" id="myImage" class="img-responsive" src="" alt="">
            </div>
            <div class="modal-footer">
                <button type="bxutton" class="btn btn-danger center-block" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  



    <div class="container-fluid">
        <br>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item active">Pengaduan</li>
            <li class="breadcrumb-item active">Selesai</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
                <b>Laporan</b>  
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pengadu</th>
                                <th>Aduan</th>
                                <th>Picture Awal</th>
                                <th>Tanggapan</th>
                                <th>Picture</th>
                                @if(Auth::user()->role == "admin")
                                <th>Aksi</th>
                                @else
                                @endif
                        </thead>
                        <tbody>
                    @isset($laporans)
                        @foreach ($laporan as $item)
                        @if($item->picture != null && $item->picture != "")
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('laporan.show', $item->id) }}">{{ $item->name }}</a></td>
                                <td><p class="no-wrap">{!! nl2br($item->aduan) !!}</p></td>
                                <td><img width="150px" height="100px"  src="{{$item->picture_awal}}" alt=""></td>
                                <td><p class="no-wrap">{!! nl2br($item->tanggapan) !!}</p></td>
                                    <td>
                                    <a href="#img" data-id="{{ $item->picture }}" class="openImageDialog thumbnail" data-toggle="modal">
                                        <img src="{{ $item->picture }}" width="200px"
                                        height="100px">
                                    </a>
                                    </td>
                                
                                @if(request()->user()->role == "admin")
                                    <td>
                                        <a href="delete-tanggapan?id={{$item->id}}" class="btn btn-outline-danger">Hapus</a>
                                        <a href="selesai/{{$item->id}}/pdf" class="btn btn-outline-success">Cetak</a>
                                    </td>
                                @endif
                            </tr>
                            @endif
                        @endforeach
                    @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script type="text/javascript">

    $(document).on("click", ".openImageDialog", function () {
        console.log('Ok');
    });


    $(document).on("click", ".openImageDialog", function () {
        var myImageId = $(this).data('id');
        $(".modal-body #myImage").attr("src", myImageId);
    });
</script>
@endsection