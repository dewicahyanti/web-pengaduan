@extends("layouts.app")
@section("title", "Data Pengadu")

@section("custom-style")

<style>
    .no-wrap {
        /* text-overflow: ellipse;
        white-space: nowrap;
        overflow: hidden; */
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
</style>

@section("content")
<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">Laporan Saya</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
            <a href="{{ route('add-pengadu') }}" class="btn btn-primary" id="createPengadu"
                style="margin-left: 1rem">
                Tambahkan Laporan
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Aduan</th>
                            <th>Picture</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($laporans as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><p class="no-wrap">{!! nl2br($item->aduan) !!}</p></td>
                        <td>
                            @if($item->picture_awal == NULL)
                            @else
                            <img width="150px" height="100px"  src="{{$item->picture_awal}}" alt=""></td>
                            @endif
                        <td>
                            <a href="{{ route('laporan.show', $item->id) }}" class="btn btn-outline-info">
                                Show
                            </a>
                            <a href="{{ url('edit-pengadu', $item->id) }}" class="btn btn-outline-danger">Edit</a>
                            <a href="{{ url('delete-pengadu', $item->id) }}" class="btn btn-outline-danger"><i class="bi bi-trash">Hapus</i></a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection