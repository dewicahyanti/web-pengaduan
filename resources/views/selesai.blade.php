@extends("layouts.app")
@section("title", "Selesai")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">Pengaduan</li>
                            <li class="breadcrumb-item active">Selesai</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                                <i class="fas fa-arrow-circle-left"></i>
                            </button>
                                Laporan 
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pengadu</th>
                                                <th>Aduan</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                <a href="#" class="btn btn-outline-danger">Hapus</a>
                                                </td>
                                            </tr>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection