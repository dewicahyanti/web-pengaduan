@extends("layouts.app")
@section("title", "Dashboard")
@section("content")
    <div class="container-fluid px-4">
        <h1 class="mt-4">Hai {{Auth::user()->name}}!</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
        <div class="col-xl-3 col-md-6">
            <div onclick="document.location.href='/menunggu'" class="card bg-primary text-white mb-4">
                <div class="card-body mx-auto">Total Pengaduan</div>
                <h1 class="card-body mx-auto">{{$laporan->total}}</h1>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>

            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div onclick="document.location.href='/ditanggapi'" class="card bg-warning text-white mb-4">
                <div class="card-body mx-auto">Pengaduan Menunggu</div>
                <h1 class="card-body mx-auto">{{$laporan->waiting}}</h1>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div onclick="document.location.href='/selesai   '" class="card bg-success text-white mb-4">
                <div class="card-body mx-auto">Pengaduan Selesai</div>
                <h1 class="card-body mx-auto">{{$laporan->finished}}</h1>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        @if(auth()->user()->role=='admin')
        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body mx-auto">Pengadu</div>
                <h1 class="card-body mx-auto">{{$pengadu}}</h1>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="{{ route('pengadu') }}">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection