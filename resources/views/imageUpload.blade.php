@extends('layouts.app')
@section("content")

<div class="section-header">
    <!-- <H1>image upload</H1>   -->
</div>

<div class="section-body">
    <div class="card">
        <div class="card-header">
            <h4>Upload Image </h4>
        </div>
            <div class="card-body">
                <div class="container">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h2>image upload</h2></div>
                            <div class="panel-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
                                <strong>{{ $message }}</strong>
                                </div>
                                <img width="50%"height="50%" src="{{Session::get('picture')}}">
                            @endif
                    
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    
                        <form action="{{route('image.upload.post')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                            <input type="hidden" name="id" id="id" value="{{$id}}">

                                <div class="col-md-6">
                                    <input type="file" name="picture" class="form-control">
                                </div>
                    
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">Upload</button>
                                    <a class="btn btn-primary" href="/ditanggapi">Back</a>
                                </div>
                    
                            </div>
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

function triggerImage(){
        var x = document.getElementById("myFile");
        var txt = "";
        if ('files' in x) {
            if (x.files.length == 0) {
                txt = "Select one or more files.";
            } else {
                for (var i = 0; i < x.files.length; i++) {
                txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                var file = x.files[i];
                    if ('name' in file) {
                        txt += "name: " + file.name + "<br>";
                    }
                    if ('size' in file) {
                        txt += "size: " + file.size + " bytes <br>";
                    }
                }
            }
        } else {
            if (x.value == "") {
                txt += "Select one or more files.";
            } else {
                txt += "The files property is not supported by your browser!";
                txt  += "<br>The path of the selected file: " + x.value;
            }
        }
        // $name = $request->input('name');
        // $getimageName = $name.'.'.$request->image->getClientOriginalExtension();
        // $request->image->move(public_path('images'), $getimageName);
        // document.getElementById("myFile").innerHTML = txt;
    }
    </script>
@endsection