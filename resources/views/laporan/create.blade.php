@extends("layouts.app")
@section("title", "Tanggapan")
@section("content")
<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">Tanggapan</li>
        <li class="breadcrumb-item active">{{ $laporan->id }}</li>
    </ol>
    <div class="card-header">
    <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
        <i class="fas fa-arrow-circle-left"></i>
    </button>
    Edit Tanggapan
    </div>
    <div class="card-body">
        <form action="/save-tanggapan/{{ $laporan->id }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <p class="m-0"><strong>Aduan :</strong></p>
                <p class="card-text">
                    {!! nl2br($laporan->aduan) !!}
                </p>
            </div>
                <br>
                <div class="form-group">
                <label for="Picture awal"><b>Picture awal :</b></label>
                @if($laporan->picture_awal)
                    <img  src="{{ asset($laporan->picture_awal) }}"  class="card-img-bottom" alt="...">
                @endif
            </div><br>
            <div class="form-group">
                <label for="tanggapan"><b>Tanggapan :</b></label>
                <textarea id="tanggapan" class="form-control" name="tanggapan" rows="5" cols="40" required>{{ $laporan-> tanggapan }}</textarea>
            </div><br>
            <br>
            {{-- <div class="form-group">
                @csrf
                <div class="row">

                <input type="hidden" name="id" id="id" value="{{$laporan->id}}">

                    <div class="col-md-6">
                        <input type="file" name="picture" class="form-control">
                    </div>
        
                    {{-- <div class="col-md-6">
                        <button type="submit" class="btn btn-success">Upload</button>
                        <a class="btn btn-primary" href="/ditanggapi">Back</a>
                    </div>
                </div> --}}
                <br>
<div class="form-group">
    <button type="submit" class="btn btn-success">Simpan</button>
    <a href="/ditanggapi" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
</div>
            </div>
            <br>
        </form>
    </div>
</div>
@endsection