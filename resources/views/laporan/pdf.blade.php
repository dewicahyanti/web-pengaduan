<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: Arial, Helvetica, sans-serif;
        /* border: 1px solid red !important; */
    }
    table {
        width: 100%;
        margin: 10px 0;
        text-align: left;
        border-spacing: 5px;
    }
    tr, th, td {
        text-align: left;
    }
    th, td {
        padding: 0;
        font-size: 12px;
    }
    h3, th, b{
        font-weight: bold;
    }

    .container {
        width: 90%;
        margin: 0 auto;
    }        
    .status_laporan {
        margin: 50px 0;
        width: 100%;
        text-align: center;
    }
    .status_laporan > h5{
        margin-bottom: 30px;
    }
    .header {
        width:  70%;
        margin: 0 auto;
        padding-top: 50px;
        padding-bottom: 30px;
        text-align: center;
    }
    .header p {
        margin: auto;
        max-width: 380px;
        font-size: 12px;
    }
    .header img{
        position: absolute;
        left: 80px;
        object-fit: contain;
    }
    .content tr > th,
    .content tr > td{
        width: 25%;
    }
    .title{
        margin-bottom: 10px;
        background-color: #eee;
    }
    .title > th{
        padding: 12px 0;
        font-weight: bold;
    }
    .footer th,
    .footer td{
        text-align: center;
    }
    .bukti-box{
        
    }
    .bukti-tanggapan{
        width: 400px;
        height: 250px;
        margin-left: 50%;
        transform: translate(-50%, 0);
    }
</style>

<?php
    $user = \App\Models\User::find($laporan->user_id);
?>



<div class="container">
    <div class="header">
        <img width="70" height="70" src="" alt="">
        <h3>PENGADUAN</h3>
        <p>Kampoeng Cireundeu, Leuwigajah, Cimahi Selatan, Cimahi, Jawa Barat 40532</p>
        <p>Telp. 021-89454790</p>
    </div>
    <table style="margin-bottom: 10px">
        <tr>
            <th>ID Pengaduan</th>
            <td><b>:</b> {{$laporan->id}} </td>
            <th>Email User</th>
            <td><b>:</b> {{$user->email}}</td>
            <th>Nama User</th>
            <td><b>:</b> {{$user->name}}</td>
        </tr>
        <tr>
            <th>No. Hp</th>
            <td><b>:</b> {{$user->no_hp}}</td>
            <th>Alamat</th>
            <td><b>:</b> {{$user->alamat}}</td>
            <th>Tgl. Aduan</th>
            <td><b>:</b> {{Carbon\Carbon::create($laporan->crated_at)->format('d M Y')}}</td>
        </tr>
    </table>
    
    <table class="content" style="margin-bottom: 10px">
        <tr class="title">
            <th colspan="4" style="text-align: center;">DETAIL PENGADUAN</th>
        </tr>
        <tr>
            <th colspan="2" >Aduan:</th>
            <td colspan="2" > {!! nl2br($laporan->aduan) !!}</td>
        </tr>
        <tr>
        <th colspan="2" >tanggapan:</th>
        <td colspan="2" > {{ ucfirst($laporan->tanggapan) }}</td>
        </tr>

        @if($laporan->picture_awal)
        <tr>    
            <th colspan="4" class="bukti-box">Picture Awal :</th>
        </tr>
        <tr>    
            <td colspan="4" class="bukti-box"><img class=" bukti-tanggapan" width="700" src="{{public_path($laporan->picture_awal)}}" alt="Bukti Tanggapan" /></td>
        </tr>
        @endif

        @if($laporan->picture)
        <tr>    
            <th colspan="4" class="bukti-box">Picture Akhir :</th>
        </tr>
        <tr>    
            <td colspan="4" class="bukti-box"><img class=" bukti-tanggapan" width="700" src="{{public_path($laporan->picture)}}" alt="Bukti Tanggapan" /></td>
        </tr>
        @endif

    </div>
    </table><br><br>
    

    <table class="content footer">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>Jakarta, {{ Carbon\Carbon::now()->format('d F Y') }}</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <th>(Admin Cireundeu)</th>
        </tr>
    </table>
</div>