@extends("layouts.app")
@section("title", "Ditanggapi")
@section('custom-style')
<style>
    .no-wrap {
        /* text-overflow: ellipse;
        white-space: nowrap;
        overflow: hidden; */
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
</style>
@section("content")
    <div class="container-fluid">
        <br>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item active">Pengaduan</li>
            <li class="breadcrumb-item active">Ditanggapi</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
                Laporan 
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pengadu</th>
                                <th>Aduan</th>
                                <th>Picture Awal</th>
                                <th>Tanggapan</th>
                                @if(Auth::user()->role == "admin")
                                <th>Aksi</th>
                                @else
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                    @isset($laporans)
                        @foreach ($laporan as $item)

                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('laporan.show', $item->id) }}" class="text-decoration-none">{{ $item->name }}</a></td>
                                <td><p class="no-wrap">{!! nl2br($item->aduan)  !!}</p></td>
                                <td><img width="150px" height="100px"  src="{{$item->picture_awal}}" alt=""></td>
                                <td><p class="no-wrap">{!! nl2br($item->tanggapan)  !!}</p></td>
                                
                                @if(request()->user()->role == "admin")
                                    <td>
                                        <a href="image-upload/{{$item->id}}" class="btn btn-outline-primary">Unggah Foto</a>
                                        <a href="{{ url('edit-ditanggapi', $item->id) }}" class="btn btn-outline-warning">Edit</a>
                                        <a href="{{ url('delete-laporan', $item->id) }}
                                            " class="btn btn-outline-danger">Hapus</a>
                                    </td>
                                @endif
                            </tr>

                            <!-- "delete-laporan?id={{$item->id}}" -->

                        @endforeach
                    @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection