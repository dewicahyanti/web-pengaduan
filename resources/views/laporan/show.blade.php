@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">Laporan Saya</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
        <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
            <h5 class="d-inline" style="margin-left: 1rem">Pengaduan</h5>
        </div>
        <div class="card-body">
                <h5 class="card-title mb-3">{{ $laporan->user->name }}</h5>
                <p class="m-0"><strong>Aduan :</strong></p>
                <p class="card-text">
                    {!! nl2br($laporan->aduan) !!}
                </p>
                @if($laporan->tanggapan)
                <p  class="m-0"><strong>Tanggapan :</strong></p>
                <p class="card-text">{!! $laporan->tanggapan !!}</p>
                @endif
        </div>
        <div class="row">
            
                @if($laporan->picture_awal)
                    <div class=col-md-6>
                    <label for="Picture awal"><b>Picture awal :</b></label>
                        <img  src="{{ asset($laporan->picture_awal) }}"  class="card-img-bottom" alt="...">
                    </div>
                @endif
                @if($laporan->picture)
                    <div class=col-md-6>
                    <label for="Picture awal"><b>Picture akhir :</b></label>
                        <img src="{{ asset($laporan->picture) }}" class="card-img-bottom" alt="...">
                    </div>
                @endif
                
            
        </div>
</div>

@endsection