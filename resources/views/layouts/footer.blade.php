<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid px-4">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; pengaduan.solusi.vip 2021</div>
            <div>
                <img src="{{URL::asset('whatsapp-logo-png-2260.png')}}" width="20px" height="20px">
                <b> Admin : 083873682676</b>
            </div>
        </div> 
    </div>
</footer>