<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <!-- Navbar Brand-->
    <a class="navbar-brand ps-3" href="/"><img height="50px" src="{{asset('download.png')}}">    Pengaduan</a>
    <!-- Sidebar Toggle-->
    <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
    <!-- Navbar-->
    <ul class="navbar-nav ms-auto me-0 me-md-3 my-2 my-md-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li>
                <form action="{{ url('profile') }}">
                <button class="btn btn-link dropdown-item text-primary" type="submit">Profile</button>
                </form>
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-link dropdown-item text-danger" type="submit">Logout</button>
                </form>
                </li>
            </ul>
        </li>
    </ul>
</nav>