<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Menu</div>
                <a class="nav-link" href="/">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                @if(auth()->user()->role == 'user')
                <a class="nav-link" href="/datapengadu">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Laporan Saya
                </a>
                @endif
                @if (Auth::user()->role != "admin")
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Pengaduan
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="/menunggu">Menunggu</a>
                        <a class="nav-link" href="/ditanggapi">Ditanggapi</a>
                        <a class="nav-link" href="/selesai">Selesai</a>
                    </nav>
                </div>
                @else
                @endif
            
                @if(Auth::user()->role == 'admin')
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Laporan
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="/menunggu">Menunggu</a>
                        <a class="nav-link" href="/ditanggapi">Ditanggapi</a>
                        <a class="nav-link" href="/selesai">Selesai</a>
                    </nav>
                </div>
                <a class="nav-link" href="/pengguna">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Pengguna
                </a>
                @endif

            </div>       
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            {{ ucfirst(Auth::user()->role) }}
        </div>
    </nav>
</div>