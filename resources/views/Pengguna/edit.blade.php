@extends("layouts.app")
@section("title", "edit-pengguna")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Pengguna</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                                <i class="fas fa-arrow-circle-left"></i>
                            </button>
                                Data Pengguna 
                            </div>
                            <div class="card-body">
                                <form action="{{ url('update-pengguna', $editPengguna->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Nama pengguna</label>
                                        <input type="text" name="nama" id="nama"
                                            class="form-control" placeholder="Nama anda" 
                                            value="{{ $editPengguna->name }}" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Email pengguna</label>
                                        <input type="text" name="email" id="email"
                                            class="form-control" placeholder="email@example.com" 
                                            value="{{ $editPengguna->email }}" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Role</label>
                                        <select name="role" id="role" required class="form-control">
                                            <option value="admin" {{ $editPengguna->role == 'admin' ? 'selected' : '' }}>Admin</option>
                                            <option value="user" {{ $editPengguna->role == 'user' ? 'selected' : '' }}>User</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                        <a href="/pengguna" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection