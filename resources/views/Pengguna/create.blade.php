@extends("layouts.app")
@section("title", "add-pengguna")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Pengguna</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                            <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                                <i class="fas fa-arrow-circle-left"></i>
                            </button>
                                Data Pengguna 
                                <a href="" class="btn btn-primary" id="createPengguna">
                                    Tambahkan Pengguna
                                </a>
                            </div>
                            <div class="card-body">
                                <form action="{{ url('save-pengguna') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="nama">Nama Pengguna</label>
                                        <input type="text" name="nama" id="nama"
                                            class="form-control" placeholder="Nama" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="email">Email Pengguna</label>
                                        <input type="text" name="email" id="email"
                                            class="form-control" placeholder="Email" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" name="password" id="password"
                                            class="form-control" placeholder="Password anda" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Level Pengguna</label>
                                        <select name="level" id="level" aria-placeholder="level" class="form-control">
                                            <option selected>Level</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Petugas">Petugas</option>
                                            <option value="Pengadu">Pengadu</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                        <a href="/pengguna" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection