@extends("layouts.app")
@section("title", "add-pengadu")
@section("content")
<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item">Laporan</li>
        <li class="breadcrumb-item active">Tambah Laporan</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
        <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
        Tambah Laporan
        </div>
        <div class="card-body">
            <form action="{{ route('save-pengadu') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="aduan">Nama Pengadu</label>
                    <input type="text" name="nama_pengadu" id="nama_pengadu"
                        class="form-control" placeholder="Nama Pengadu" value="{{ $user->name }}" readonly>
                </div>
                <br>
                <div class="form-group">
                    <label for="aduan">Aduan</label>
                    <textarea type="text" name="aduan" id="aduan"
                        class="form-control" placeholder="Aduan" required rows="7"></textarea>
                </div>
                <br>
                <div class="row">



                        <div class="col-md-6">
                            <input type="file" name="picture_awal" id="picture_awal" class="form-control  @error('picture_awal') is-invalid @enderror">
                            @error('picture_awal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <br>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Lapor</button>
                    <a href="/laporan" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection