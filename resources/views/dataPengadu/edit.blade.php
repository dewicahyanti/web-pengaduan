@extends("layouts.app")
@section("title", "edit-pengadu")
@section("content")
<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Data Pengadu</li>
    </ol>
    <div class="card-header">
        <!-- <h1>testing nih bro tes tes tes nama saya dewi cahyanti</h1> -->
        <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
            <i class="fas fa-arrow-circle-left"></i>
        </button>
            Edit Pengguna 
        </div>
        <div class="card-body">
            <form action="{{ url('update-pengadu', $editPengadu)}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama Pengadu</label>
                    <input disabled type="text" name="nama_pengadu" id="nama_pengadu"
                        class="form-control" placeholder="Nama Pengadu" value="{{auth()->user()->name}}">
                </div><br>

                <div class="form-group">
                    <label for="aduan">Aduan</label>
                    <textarea type="text" name="aduan" id="aduan" class="form-control" 
                    required rows="7"><?php echo htmlspecialchars($editPengadu->aduan); ?></textarea>
                </div><br>

                <div class="row">
                        <div class="col-md-6">
                            <input type="file" name="picture_awal" id="picture_awal" class="form-control">
                        </div>
                    </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <a href="/datapengadu" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection