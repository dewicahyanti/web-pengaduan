@extends("layouts.app")
@section("title", "Menunggu")

@section('custom-style')
<style>
    .no-wrap {
        /* text-overflow: ellipse;
        white-space: nowrap;
        overflow: hidden; */
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
</style>

@section("content")
<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">Pengaduan</li>
        <li class="breadcrumb-item active">Menunggu</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
        <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
            Laporan 
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pengadu</th>
                            <th>Aduan</th>
                            <th>Picture Awal</th>
                            @if(Auth::user()->role == "admin")
                            <th>Aksi</th>
                            @else
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($laporan as $lapor)
                        <tr>
                            @if($lapor->tanggapan == NULL)
                            <td>{{ $loop->iteration }}</td>
                            <td><a href="{{ route('laporan.show', $lapor->id) }}" class="text-decoration-none">{{ $lapor->name }}</a></td>
                            <td><p class="no-wrap">{!! nl2br($lapor->aduan)  !!}</p></td>
                            <td><img width="150px" height="100px"  src="{{$lapor->picture_awal}}" alt=""></td>
                            @if(Auth::user()->role == "admin")
                            <td>
                                <a href="add-tanggapan/{{$lapor->id}}" class="btn btn-outline-primary">Tanggapi</a>
                                <a href="delete-tanggapan?id={{$lapor->id}}" class="btn btn-outline-danger">Hapus</a>
                            </td>
                            @endif
                            @endif
                        </tr>
                    @endforeach
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection