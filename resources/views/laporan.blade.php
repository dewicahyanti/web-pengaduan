@extends("layouts.app")
@section("title", "laporan")
@section("content")
<!-- Modal Picture
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="modal fade" id="modal_image" aria-labelledby="modal_image" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_image">Image Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="picture" method="post" enctype="multipart/form-data">
                @csrf
                
                <div class="modal-body">
                    <div class="form-group">
                            <label for="">Upload File</label><br>
                            <input multiple type="file" name="picture[]" id="picture">
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn_submit" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button id="btnAdd" type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i></i> Add</button>
                </div>
            </form>
        </div>
    </div>
</div> -->





<div class="container-fluid">
    <br>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        <li class="breadcrumb-item active">Laporan</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
        <button action="action" onclick="window.history.go(-1); return false;" type="submit" data-original-title="Kembali" title="Kembali" data-toggle="tooltip" class="btn btn-dark" >
                <i class="fas fa-arrow-circle-left"></i>
            </button>
            Laporan 
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pengadu</th>
                            <th>Aduan</th>
                            <th>Picture</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($laporan as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->aduan }}</td>
                            <td>{{ $item->picture }}</td>
                            <td>
                                <a href="{{ url('delete-laporan', $item->id_laporan) }}" class="btn btn-outline-danger">Hapus</a>
                                <a href="{{ url('add-tanggapan', $item->id_laporan) }}" class="btn btn-outline-secondary">{{ $item->tanggapan ? "Edit tanggapan" : "Tanggapi" }}</a>
                                {{-- <a href="{{ route('image.upload', $item->id_laporan) }}" class="btn btn-outline-primary" >Upload Image</a> --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection