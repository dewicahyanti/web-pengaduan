<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    DashboardController,
    penggunaController,
    datapengaduController,
    laporanController,
    jenisaduanController,
    AuthController,
    tanggapanController,
    MenungguController,
    ProfileController
};

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('login.post');
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('profile', [ProfileController::class, 'update'])->name('profile.update');
    
    Route::get('/pengguna', [penggunaController::class, 'index']);
    Route::get('add-pengguna', [penggunaController::class, 'create'])->name('add-pengguna');
    Route::post('save-pengguna', [penggunaController::class, 'store'])->name('save-pengguna');
    Route::get('edit-pengguna/{id}', [penggunaController::class, 'edit'])->name('edit-pengguna');
    Route::post('update-pengguna/{id}', [penggunaController::class, 'update'])->name('update-pengguna');
    Route::get('delete-pengguna/{id}', [penggunaController::class, 'destroy'])->name('delete-pengguna');

    Route::get('/laporan', [laporanController::class, 'index']);
    Route::get('/add-laporan/{id}', [laporanController::class, 'create'])->name('add-laporan');
    // Route::get('/edit-tanggapan')
    Route::post('save-laporan', [laporanController::class, 'store'])->name('save-laporan');
    Route::get('delete-laporan/{id}', [laporanController::class, 'destroy'])->name('delete-laporan');
    Route::get('laporan/{id}', [laporanController::class, 'show'])->name('laporan.show');
    
    Route::get('/datapengadu', [datapengaduController::class, 'index']);
    Route::get('/pengadu', [datapengaduController::class, 'pengadu'])->name('pengadu');
    Route::get('add-pengadu', [datapengaduController::class, 'create'])->name('add-pengadu');
    Route::post('save-pengadu', [datapengaduController::class, 'store'])->name('save-pengadu');
    Route::get('edit-pengadu/{id}', [datapengaduController::class, 'edit'])->name('edit-pengadu');
    Route::post('update-pengadu/{id}', [datapengaduController::class, 'update'])->name('update-pengadu');
    Route::get('delete-pengadu/{id}', [datapengaduController::class, 'destroy'])->name('delete-pengadu');
    
    Route::get('/add-tanggapan/{id}', [tanggapanController::class, 'create'])->name('add-tanggapan'); 
    Route::get('/edit-tanggapan/{id}', [tanggapanController::class, 'edit'])->name('edit-tanggapan'); 
    Route::post('/save-tanggapan/{id}', [tanggapanController::class, 'update'])->name('save-tanggapan');
    Route::get('/delete-tanggapan', [tanggapanController::class, 'destroy'])->name('delete-tanggapan');
    Route::get('/selesai', [tanggapanController::class, 'index']);
    Route::get('selesai/{laporan:id}/pdf', [tanggapanController::class, 'print_pdf'])->name('selesai.pdf');
    
    Route::get('/ditanggapi', [MenungguController::class, 'index']);
    Route::get('/menunggu', [MenungguController::class, 'all']);
    Route::get('/edit-ditanggapi/{id}', [tanggapanController::class, 'editDitanggapi'])->name('edit-ditanggapi');
    Route::post('image-upload/{id?}', [App\Http\Controllers\laporanController::class, 'imageUploadPost'])->name('image.upload.post');
    Route::get('image-upload/{id?}', [App\Http\Controllers\laporanController::class, 'imageUpload'])->name('image.upload');
});