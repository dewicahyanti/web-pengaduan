<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    public $timestamps = true;

    protected $table = "laporan";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'user_id',
        'aduan',
        'picture_awal',
        'tanggapan',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
