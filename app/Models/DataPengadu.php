<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPengadu extends Model
{
    public $timestamps = true;

    protected $table = "pengadu";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'nama_pengadu',
        'jenis_kelamin',
        'no_hp',
        'alamat',
    ];
}
