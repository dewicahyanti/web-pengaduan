<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Laporan;
use PDF;

class tanggapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laporans = Laporan::where('tanggapan', '!=', NULL)->where('picture', '!=', NULL)->get();
        $laporan = Laporan::select(
            'laporan.*',
            'users.*',
            'laporan.id as id'
        )->join(
            'users',
            'laporan.user_id',
            '=',
            'users.id'
        )->where('laporan.tanggapan', '!=', NULL
        )->where('laporan.picture', '!=', NULL
        )->get();
        return view('ditanggapi', compact('laporans','laporan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $laporan = Laporan::find($id);
        // dd($laporan);
        return view('tanggapan.create', compact('laporan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tanggapan = Laporan::find($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laporan = Laporan::find($id);
        // dd($laporan);
        return view('tanggapan.create', compact('laporan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Laporan::find($id);
        $user->tanggapan = $request->tanggapan;
        $user->save();
        
        return redirect('/ditanggapi');
    }

    public function editDitanggapi($id)
    {
        $laporan = Laporan::find($id);
        return view('laporan.create', compact('laporan'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $deleteTanggapan = Laporan::findorfail($id);
        $deleteTanggapan->delete();
        return back();
    }


    public function print_pdf(Laporan $laporan) {
        $pdf = PDF::loadview('laporan/pdf', ['laporan' => $laporan]);

        return $pdf->stream('Pengaduan.pdf');
    }
}
