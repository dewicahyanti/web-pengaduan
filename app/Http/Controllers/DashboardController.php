<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Laporan, User};

class DashboardController extends Controller
{
    public function index() {
        $user = auth()->user();
        $total; $waitng; $finished;

        if($user->role == 'admin') {
            $waiting    = Laporan::where('tanggapan', NULL)->count();
            $finished   = Laporan::where('tanggapan', '!=', NULL)->count();
            $pengadu    = User::where('role', 'user')->count();
        } 

        if($user->role == 'user') {
            $waiting    = Laporan::where('tanggapan', NULL)->count();
            $finished   = Laporan::where('tanggapan', '!=', NULL)->count();
            $pengadu    = User::where('role', 'user')->count();
        }
        
        $laporan    = json_decode(collect([
            "total" => $waiting + $finished,
            "waiting" => $waiting,
            "finished" => $finished
        ]));

        return view('dashboard', compact('laporan', 'pengadu'));
    }       
}
