<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Auth, User, Laporan, DataPengadu};

class datapengaduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id  = auth()->user()->id;
        $laporans = Laporan::where('user_id', $id)->get();
        return view('datapengadu', compact('laporans'));
    }


    public function pengadu() {
        $pengadu = User::where('role', 'user')->get();
        return view('datapengadu.pengadu', compact('pengadu'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $id = $request->id;
        // $laporan = Laporan;
        $user = request()->user();
        return view('dataPengadu.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'picture_awal' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $laporan=Laporan::create([
            'user_id' => auth()->user()->id,
            'aduan' => $request->aduan,
        ]);
        $id=$laporan->id;

        $laporan_image=Laporan::find($id);
        


        if($request->picture_awal){
            $imageName = time().'.'.$request->picture_awal->extension();

            $request->file('picture_awal')->move(public_path('image_awal'), $imageName);
            laporan::where('id',$laporan_image->id)->update(['picture_awal'=> 'image_awal/'.$imageName]);

        }

        return redirect('datapengadu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editPengadu = Laporan::find($id);
        // dd($editPengadu); 
        return view('dataPengadu.edit', compact('editPengadu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editPengadu = Laporan::findorfail($id);
        $editPengadu->update($request->all());
        $laporan_image=Laporan::find($id);
        $request->validate([
            'picture_awal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->picture_awal){
            $imageName = time().'.'.$request->picture_awal->extension();

            $request->file('picture_awal')->move(public_path('image_awal'), $imageName);
            laporan::where('id',$laporan_image->id)->update(['picture_awal'=> 'image_awal/'.$imageName]);
  
        }
        return redirect('datapengadu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletePengadu = Laporan::findorfail($id);
        $deletePengadu->delete();
        return back();
    }



}
