<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Laporan};

class MenungguController extends Controller
{
    //
    public function index() 
    {
        $laporans = Laporan::all();
        $laporan = Laporan::where('laporan.tanggapan','!=',NULL
        )->where('laporan.tanggapan','!=',""
        )->where('laporan.picture',NULL
        )->select(
            'laporan.*',
            'users.*',
            'laporan.id as id'
        )->join(
            'users',
            'laporan.user_id',
            '=',
            'users.id'
        )->get();
        // dd($laporan);
        return view('laporan.semua', compact('laporans','laporan'));
    }

    public function all(){
        
        $laporans = Laporan::where('tanggapan', NULL)->get();
        $laporan = Laporan::where('laporan.tanggapan',NULL
        )->select(
            'laporan.*',
            'users.*',
            'laporan.id as id'
        )->join(
            'users',
            'laporan.user_id',
            '=',
            'users.id'
        )->get();
        // dd($laporan);
        return view('menunggu', compact('laporans','laporan'));
    }
}
