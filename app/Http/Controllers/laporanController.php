<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Laporan, DataPengadu};

class laporanController extends Controller {
    public function index() {
        $laporans = Laporan::all();
        $laporan = Laporan::select(
            'laporan.*',
            'users.*',
            'laporan.id as id_laporan',
        )->join(
            'users',
            'laporan.user_id',
            '=',
            'users.id'
        )->get();
        // dd($laporan); 
        return view('laporan', compact('laporans','laporan'));
    }

    
    public function create($id) {
        $tambahlaporan = DataPengadu::find($id);
        return view('laporan.create', compact('tambahlaporan'));
    }

    
    public function store(Request $request) {
        Laporan::create([
            'user_id' => auth()->user()->id,
            'aduan' => $request->aduan,
        ]);

        return redirect('laporan');
    }

    
    public function show($id) {
        $laporan = Laporan::find($id);
        
        return view('laporan.show', compact('laporan'));
    }

    
    public function edit($id) {
        //
    }

    
    public function update(Request $request, $id) {
        //
    }

    
    public function destroy($id) {
        $deleteLaporan = Laporan::findorfail($id);
        $deleteLaporan->delete();
        return back();
    }

    public function imageUpload($id)
    {
        return view('imageUpload', compact('id'));
    }

    public function imageUploadPost(Request $request)
    {
        $id = $request->id;
        
        $laporan=Laporan::find($id)->first();
        $laporan_image=Laporan::find($id);
        // $laporan2=Laporan::where($laporan->id,$id);
        // dd($laporan);

        $request->validate([
            'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->picture){
            $imageName = time().'.'.$request->picture->extension();

            $request->file('picture')->move(public_path('image_upload'), $imageName);
            laporan::where('id',$laporan_image->id)->update(['picture'=> 'image_upload/'.$imageName]);
            /* Store $imageName name in DATABASE from HERE */
            // $laporan2->picture = $request->picture;
            // $laporan2->save();
        }
        return redirect('/selesai');
        // return view('imageUpload',compact('$imageName'));
            // ->with('success','You have successfully upload image.')
            // ->with('image',$imageName);
        
        // return back()
        //     ->with('success','You have successfully upload image.')
        //     ->with('image',$imageName); 
        
    }
}
